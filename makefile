.ONESHELL:

DATE = $(shell date +'%s')
CONTAINER_REGISTRY_URL = "jarvisnetworkcoreacr.azurecr.io/jarvis-network/apps/vault-etheurm"
docker-build:
	docker build --build-arg always_upgrade="$(DATE)" -t $(CONTAINER_REGISTRY_URL) .

run:
	docker-compose -f docker/docker-compose.yml up --build --remove-orphans

all: docker-build run
